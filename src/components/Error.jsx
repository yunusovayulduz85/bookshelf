import React from 'react'
import BackgroundImg from "../assets/backgImg.png"
import NotFound from "../assets/notFound.png";
import Button from './Button';
import { Link } from 'react-router-dom';
const Error = () => {
  return (
    <div className='h-screen overflow-y-hidden'>
      <img className='absolute h-screen w-70% z-10' src={BackgroundImg} />
      <div className=' relative z-20 w-screen h-screen flex items-center justify-center flex-col'>
        <img src={NotFound}/>
        <div className='flex gap-2 mt-4 items-center justify-center w-96'>
          <Button><Link to={"books"}>Go Home Page</Link></Button>
          <Button className='w-full border mt-5   rounded-md py-2.5 font-medium fs-base font-Mulish'>Reolad Page</Button>
        </div>
      </div>
    </div>
  )
}

export default Error