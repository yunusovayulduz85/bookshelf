import React, { useState } from 'react'
import BackgroundImg from "../assets/backgImg.png"
import { Link, useNavigate } from 'react-router-dom';
import Button from './Button';
import { toast } from 'react-toastify';
const SignIn = () => {
  const navigate = useNavigate()
  const [name, setName] = useState("")
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const validate = () => {
    var result = true;
    if (name === "" || name === null) {
      result = false;
      alert("Please Enter Name")
    }
    if (username === "" || username === null) {
      result = false;
      alert("Please Enter userName")
    }
    if (email === "" || email === null) {
      result = false;
      alert("Please Enter email")
    }
    return result;
  }
  const handleSubmitSignIn = (e) => {
    e.preventDefault();
    if (validate()) {
      fetch('http://localhost:3005/posts')
        .then((res) => res.json())
        .then((resp) => {
          // if (resp.name === name && resp.username===username && resp.email===email) {
            alert("Success!")
            navigate("/books")
          // }
        }).catch((err) => {
          alert("Login Failed due to : " + err.massage)
        })
    }

  }
  return (
    <div className='h-screen overflow-y-hidden'>
      <img className='absolute h-screen w-70% z-10' src={BackgroundImg} />
      <div className='w-screen h-screen flex items-center justify-center'>
        <form className='z-20 relative bg-white  px-5 py-7 rounded-lg shadow-md w-96' onSubmit={handleSubmitSignIn}>
          <h1 className='font-Mulish text-center text-3xl font-bold mb-7'>Sign In</h1>

          <button className=' border mb-4 py-2.5 px-6 rounded border-gray-900 w-full'>
            <Link to={"/books"} className='flex items-center gap-2'>
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-google" viewBox="0 0 16 16">
                <path d="M15.545 6.558a9.42 9.42 0 0 1 .139 1.626c0 2.434-.87 4.492-2.384 5.885h.002C11.978 15.292 10.158 16 8 16A8 8 0 1 1 8 0a7.689 7.689 0 0 1 5.352 2.082l-2.284 2.284A4.347 4.347 0 0 0 8 3.166c-2.087 0-3.86 1.408-4.492 3.304a4.792 4.792 0 0 0 0 3.063h.003c.635 1.893 2.405 3.301 4.492 3.301 1.078 0 2.004-.276 2.722-.764h-.003a3.702 3.702 0 0 0 1.599-2.431H8v-3.08h7.545z" />
              </svg>
              <span className='font-Mulish text-base font-medium text-gray-900'>Continue with Google</span>
            </Link>

          </button>
          <button className='w-full  border mb-4 py-2.5 px-6 rounded border-gray-900'>
            <Link to={"/books"} className='flex items-center gap-2'>
             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
              <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951" />
            </svg>
            <span className='font-Mulish text-base font-medium text-gray-900'>
              Continue with Facebook
            </span>
            </Link>
           
          </button>
          <div className='flex justify-center gap-2 items-center'>
            <div className='bg-gray-500 w-full h-1px'></div>
            <p className='font-Mulish font-normal text-xs'>OR</p>
            <div className='bg-gray-500 w-full h-1px'></div>
          </div>
          <div className='mt-5'>
            <label className='font-Mulish font-medium'>Your name</label> <br />
            <input className='w-full border border-gray-400 py-3 px-4 rounded-md' placeholder='Enter your name' value={name} onChange={(e) => setName(e.target.value)} />
          </div>
          <div>
            <label className='font-Mulish font-medium'>Your username</label> <br />
            <input className='w-full border border-gray-400 py-3 px-4 rounded-md' placeholder='Enter your username' value={username} onChange={(e) => setUsername(e.target.value)} />
          </div>
          <div className='my-3'>
            <label className='font-Mulish font-medium'>Your email</label> <br />
            <input className='w-full border border-gray-400 py-3 px-4 rounded-md' placeholder='Enter your email' value={email} onChange={(e) => setEmail(e.target.value)} />
          </div>
          <div className='flex justify-center'>
            <div className='mt-5 w-full'>
              <Button>Sign In</Button>
            </div>
            {/* <button className='border mt-5 w-full bg-additionColor text-white rounded-md py-2.5 font-medium fs-base font-Mulish'>Sign In</button> */}
          </div>
          <p className='font-Mulish text-center font-light'>
            Already signed up? <Link to={"/signUp"} className='text-additionColor'>Go to sign up.</Link>
          </p>
        </form>
      </div>
    </div>
  )
}

export default SignIn