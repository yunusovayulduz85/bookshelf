import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import SignUp from '../components/SignUp'
import SignIn from '../components/SignIn'
import Books from '../components/Books'
import Error from '../components/Error'
import Home from '../components/Home'
import { ToastContainer } from 'react-toastify'

const Router = () => {
    return (
        <>
        <ToastContainer theme="colored"></ToastContainer>
         <BrowserRouter>
            <Routes>
                <Route index element={<SignUp />} />
                <Route path='signUp' element={<SignUp />} />
                <Route path='signIn' element={<SignIn />} />
                <Route path='books' element={<Home/>}>
                    <Route index element={<Books />} />
                </Route>
                <Route path='*' element={<Error />} />
            </Routes>
        </BrowserRouter>
        </>
       
    )
}

export default Router