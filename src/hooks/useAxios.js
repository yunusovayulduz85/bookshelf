import React, { useEffect, useState } from 'react'
import axios from 'axios';
axios.defaults.baseURL = "http://localhost:3006";
const useAxios = (url) => {
    const [res, setRes] = useState([]);
    const [error, setError] = useState("");
    const [loader, setLoader] = useState(true)
    useEffect(() => {
        return async () => {
            try {
                const res = await axios.get(url);
                setRes(res.data);
            } catch (err) {
                setError(err.massage)
            } finally {
                setLoader(false)
            }
        }
    }, [])
    return { res, error, loader }
}

export default useAxios