module.exports = {
    plugins: ['prettier-plugin-tailwindcss'],
    singleQuote: true,
    tailwindAttributes: ['myClassList'],
}
